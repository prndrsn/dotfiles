" Detect environment/runtime {{{
let s:is_unix = has('unix')
let s:is_windows = has('win32') || has('win64')
let s:is_cygwin = has('win32unix')
let s:is_mac = has('mac')
let s:is_nvim = has('nvim')
let s:has_ack = executable('ack')
let s:has_ag = executable('ag')
" }}}

let s:settings = {}
let s:settings.default_indent = 2
let s:settings.max_column = 120
let s:settings.enable_cursorcolumn = 0
let s:settings.colorscheme = 'gruvbox'

set nocompatible              " be iMproved, required
filetype off                  " required
set mouse=a
set clipboard=unnamed " Solves a lot of clipboard/primary selection headaches
set autowrite " Makes buffer switching less annoying by autosaving
scriptencoding utf8
set enc=utf-8
setl fileencoding=utf-8
setl fileencodings=utf-8,latin1,default

let mapleader = "-"

" Base configuration {{{
if has('autocmd')
  filetype plugin indent on
endif
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif
if &shell =~# 'fish$'
  set shell=/bin/bash
endif
" }}}

set timeoutlen=200
set ttimeoutlen=0

set noerrorbells
set novisualbell
set t_vb=

" nvim specific configuration
if s:is_nvim
  set backupdir=~/.local/share/nvim/swap "fix nvim issue
  let g:python_host_prog = '/usr/bin/python'
  let g:python3_host_prog = '/usr/bin/python3'
endif

"" Appearance {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set backspace=indent,eol,start
set autoindent
set expandtab
set smarttab
let &tabstop=s:settings.default_indent
let &softtabstop=s:settings.default_indent
let &shiftwidth=s:settings.default_indent

set list
set listchars=tab:▸\ ,trail:•,extends:❯,precedes:❮

set shiftround
set linebreak
let &showbreak='↪'
set nowrap

set scrolloff=5
set sidescroll=1
set sidescrolloff=10
set display+=lastline

set virtualedit=all

set title
set splitright
set t_Co=256
set ambiwidth=single
set background=dark
"colorscheme charredtoast256
"colorscheme gruvbox
"set showtabline=0

" Search
set hlsearch
set incsearch
set ignorecase
set smartcase

set laststatus=2
set ruler
set showcmd
set number
set relativenumber number
set colorcolumn=80
set cursorline
autocmd WinLeave * setlocal nocursorline
autocmd WinEnter * setlocal cursorline
let &colorcolumn=s:settings.max_column
if s:settings.enable_cursorcolumn
  set cursorcolumn
  autocmd WinLeave * setlocal nocursorcolumn
  autocmd WinEnter * setlocal cursorcolumn
endif

" Folding
set foldenable
set foldcolumn=1
set foldmethod=syntax
set foldlevel=1
autocmd FileType html,xhtml setlocal foldmethod=indent
let g:xml_syntax_folding=1

" }}}

" GUI options {{{
"set guioptions-=m
set guioptions-=m   "hide menu bar
"set guioptions-=t    "tear off menu items
set guioptions-=T    "toolbar icons
"set guioptions-=l    "remove left-hand scrollbars
"set guioptions-=L
"set guioptions-=r    "remove right-hand scrollbars
"set guioptions-=R
"set guioptions-=b  "remove bottom scrollbar
"set guifont=Source\ Code\ Pro\ for\ Powerline\ Semibold\ 10
set guifont=Source\ Code\ Pro\ for\ Powerline\ Medium\ 12
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Plugs and plugin management
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"" vim-plug
call plug#begin('~/.vim/plugged')

"" Code plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"" Generic code completion etc
"Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer --tern-completer --gocode-completer' } " {{{
  ""let g:ycm_filetype_whitelist = {'*' : 1}
  "let g:ycm_filetype_blacklist = {'css' : 1, 'html' : 1, 'xhtml' : 1, 'txt' : 1, 'text' : 1, 'tex' : 1, 'markdown' : 1}
  "let g:ycm_filetype_specific_completion_to_disable = {'css' : 1, 'html' : 1, 'xhtml' : 1}
  ""let g:ycm_autoclose_preview_window_after_completion = 1
  ""let g:ycm_autoclose_preview_window_after_insertion = 1
  "" <Tab> breaks Emmet even when html/css is blacklisted
  "let g:ycm_seed_identifiers_with_syntax = 1
  "let g:ycm_collect_identifiers_from_tags_files = 1
  "let g:ycm_key_list_select_completion = ['<Down>']
  "let g:ycm_key_list_previous_completion = ['<Up>']
" }}}
"Plug 'rdnetto/YCM-Generator'
Plug 'SirVer/ultisnips' " {{{
  let g:UltiSnipsExpandTrigger="<tab>"
  let g:UltiSnipsJumpForwardTrigger="<tab>"
  let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
  let g:UltiSnipsEditSplit="vertical"
  " }}}
Plug 'honza/vim-snippets'
Plug 'Shougo/deoplete.nvim' " {{{
  let g:deoplete#enable_at_startup = 1
  let g:deoplete#enable_smart_case = 1
 " }}}
Plug 'zchee/deoplete-go', { 'do': 'make'}
Plug 'nsf/gocode', { 'rtp': 'vim', 'do': '~/.config/nvim/plugged/gocode/vim/symlink.sh' }
Plug 'artur-shaik/vim-javacomplete2' " {{{
  autocmd FileType java setlocal omnifunc=javacomplete#Complete
 " }}}

"" Linting, syntax checking
"Plug 'uarun/vim-protobuf'
Plug 'wookiehangover/jshint.vim'
Plug 'scrooloose/syntastic' " {{{
  let g:syntastic_error_symbol = '✗'
  let g:syntastic_style_error_symbol = '✠'
  let g:syntastic_warning_symbol = '∆'
  let g:syntastic_style_warning_symbol = '≈'
  let g:syntastic_always_populate_loc_list = 1
  let g:syntastic_auto_loc_list = 1
  let g:syntastic_check_on_open = 1
  let g:syntastic_check_on_wq = 0
  set statusline+=%#warningmsg#
  set statusline+=%{SyntasticStatuslineFlag()}
  set statusline+=%*
" }}}
Plug 'marijnh/tern_for_vim', { 'do': 'npm install' }
Plug 'solarnz/thrift.vim'
Plug 'dbakker/vim-lint'

"" Syntax highlighting
"Plug 'sudar/vim-arduino-syntax'
Plug 'dag/vim-fish' " {{{
  autocmd FileType fish compiler fish
  autocmd FileType fish setlocal foldmethod=expr
" }}}
Plug 'tfnico/vim-gradle'
Plug 'PotatoesMaster/i3-vim-syntax' " {{{
  autocmd BufEnter *i3/config setlocal filetype=i3
  autocmd FileType i3 setlocal commentstring=#\ %s
" }}}
Plug 'jelera/vim-javascript-syntax'
Plug 'tpope/vim-markdown'
Plug 'rodjek/vim-puppet'
Plug 'tpp.vim'

"" Code formatting/visual aides
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'Chiel92/vim-autoformat'
Plug 'Yggdroot/indentLine' " {{{
  let g:indentLine_char = '│'
" }}}
Plug 'majutsushi/tagbar'  " {{{
nmap <F4> :TagbarToggle<CR>
" }}}
Plug 'TagHighlight'
Plug 'Raimondi/delimitMate' " Autoclose brackets etc
Plug 'bitc/vim-bad-whitespace' " {{{
  map <Leader>b :ToggleBadWhitespace<CR>
" }}}
Plug 'scrooloose/nerdcommenter'
Plug 'luochen1990/rainbow' " {{{
  let g:rainbow_active = 1
" }}}

"" Language specific
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"" Pandoc
"Plug 'vim-pandoc/vim-pandoc'
"Plug 'vim-pandoc/vim-pandoc-syntax'

"" Arduino
Plug 'jplaut/vim-arduino-ino'
  "let g:vim_arduino_auto_open_serial = 1
  "let g:vim_arduino_library_path = '/usr/share/arduino/hardware/arduino/cores/arduino'

"" Clojure
Plug 'tpope/vim-fireplace'
Plug 'tpope/vim-salve'
"Plug 'tpope/vim-classpath'

"" Golang
Plug 'fatih/vim-go' " {{{
  "let g:go_fmt_command = "goimports"
" }}}

"" LaTeX
Plug 'lervag/vimtex'
"Plug 'LaTeX-Suite-aka-Vim-LaTeX' " {{{
  "set grepprg=grep\ -nH\ $*
  "set shellslash " This is for Windows
  "let g:tex_flavor='latex'
  "let g:tex_fold_enabled = 1
  "let g:Tex_DefaultTargetFormat='pdf'
  "let g:Tex_ViewRule_pdf = 'zathura'
  "let g:Tex_ViewRule_dvi = 'xdvi'
  "let g:Tex_ViewRule_ps = 'zathura'
  "let g:Tex_CompileRule_pdf='pdflatex -interaction=nonstopmode $*'
  "let g:Tex_MultipleCompileFormats='dvi,pdf'
  "let g:TexAutoFolding = 1
  "let g:Tex_FoldedEnvironments='verbatim,comment,eq,gather,align,figure,table,thebibliography,keywords,abstract,titlepage'
  "let g:Tex_FoldedSections='part,chapter,section,%%fakesection,subsection,subsubsection,paragraph'
" }}}

"" Web (HTML, CSS, XML etc
Plug 'mattn/emmet-vim' " {{{
  let g:emmet_install_global = 0
  let g:user_emmet_leader_key='<C-M>'
  let g:use_emmet_complete_tag = 1
  autocmd FileType html,xhtml,htmldjango,css,scss,sasstt2,tt2html imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")
" }}}

"" Java
Plug 'dansomething/vim-eclim' " {{{
  nmap <Leader>t :ProjectTreeToggle<CR>
  let g:EclimProjectTreeAutoOpen = 1
  let g:EclimProjectTreeExpandPathOpen = 1
  let g:EclimBrowser='firefox'
  let g:EclimCompletionMethod = 'omnifunc'
  let g:EclimJavaCompleteCaseSensitive = 1
" }}}
"" Tools
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"" Email
"Plug 'felipec/notmuch-vim'
"Plug 'guyzmo/notmuch-abook'
"Plug 'yuratomo/gmail.vim'

"" Notes
Plug 'mrtazz/simplenote.vim' " {{{
  "let g:SimplenoteVertical = 1
" }}}

"" Git
Plug 'github/gitignore'
Plug 'airblade/vim-gitgutter' " {{{
  let g:gitgutter_enabled = 1
" }}}
Plug 'tpope/vim-fugitive' " Git commands and many other tools
Plug 'idanarye/vim-merginal' " Extends fugitive with a merge tool
Plug 'Xuyuanp/nerdtree-git-plugin' " Mark git changes in NERDTree
Plug 'gregsexton/gitv' " Repo viewer

"" Tmux integration
Plug 'tpope/vim-tbone'
Plug 'tmux-plugins/vim-tmux' " syntax highlighting, jump to man page entry
Plug 'edkolev/tmuxline.vim'

"" Miscellaneous
"Plug 'tpope/vim-pastie' " Send to pastebin
Plug 'tpope/vim-dispatch' " Asynchronous build/test dispatcher
Plug 'blindFS/vim-taskwarrior' " taskwarrior interface
Plug 'itchyny/calendar.vim' " {{{
  let g:calendar_google_calendar = 1
  let g:calendar_google_task = 1
  let g:calendar_frame = "unicode"
" }}}

"" Vim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"" General
Plug 'Shougo/vimproc.vim', { 'do': 'make' } " required for some plugins
Plug 'tpope/vim-sensible' " Sensible defaults
"Plug 'junegunn/fzf.vim', { 'dir': '~/.fzf', 'do': './install --all' } " fzf integration

"" Appearance
Plug 'morhetz/gruvbox' " {{{
  "let g:gruvbox_contrast_dark='hard'
" }}}
Plug 'powerline/fonts', { 'do': './install' }
Plug 'bling/vim-airline' " {{{
  let g:airline_powerline_fonts  = 1
  let g:airline#extensions#tabline#enabled = 1
" }}}
Plug 'bling/vim-bufferline'
Plug 'edkolev/promptline.vim'
Plug 'flazz/vim-colorschemes'
"Plug 'EasyColour'
"Plug 'https://prndrsn@bitbucket.org/prndrsn/charredtoast256.git' " My slightly modified burnttoast256 theme

"" Buffers and tabs
Plug 'jeetsukumaran/vim-buffergator' " {{{
  let g:buffergator_suppress_keymaps = '1'
  let g:buffergator_viewport_split_policy = 'B'
  "let g:buffergator_hsplit_size = '5'
  let g:buffergator_autodismiss_on_select = '0'
  noremap <F3> :BuffergatorToggle<cr>
" }}}
Plug 'zhaocai/GoldenView.Vim' " {{{
  "let g:goldenview__enable_default_mapping = 0
  let g:goldenview__enable_at_startup = 1
  nmap <F12> <Plug>GoldenViewResize
" }}}

"" Editing tools
Plug 'terryma/vim-multiple-cursors' " {{{ Edit multiple lines/words simultaneously
 let g:multi_cursor_next_key = '<C-n>'
 let g:multi_cursor_prev_key = '<C-p>'
 let g:multi_cursor_skip_key = '<C-x>'
 let g:multi_cursor_quit_key = '<Esc>'
" }}}
Plug 'tpope/vim-speeddating' " Quickly increment/decrement dates/numbers
Plug 'tpope/vim-surround' " Add or change brackets etc
Plug 'tpope/vim-repeat' " Adds repeat action to plugin commands
Plug 'simnalamburt/vim-mundo' " {{{
  let g:mundo_right = '1'
  let g:mundo_map_move_older = 'k'
  let g:mundo_map_move_newer = 'i'
  map <F5> :MundoToggle<CR>
" }}}

"" Navigation
Plug 'Lokaltog/vim-easymotion' " {{{
  nmap F <Plug>(easymotion-w)
  nmap <C-f> <Plug>(easymotion-b)
  nmap f <Plug>(easymotion-bd-w)
" }}}
Plug 'vim-scripts/vimcommander' " {{{
  noremap <silent> <Leader>v :cal VimCommanderToggle()<CR>
" }}}
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] } " {{{
  autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
  let NERDTreeShowLineNumbers=1
  let NERDTreeShowBookmarks=1
  let NERDTreeMinimalUI=1
  let NERDTreeCascadeOpenSingleChildDir=1
  nnoremap <F1> :NERDTreeToggle<CR>
  nnoremap <F2> :NERDTreeFind<CR>
" }}}

"" Session management
Plug 'tpope/vim-obsession' " Autosave sessions

" All of your Plugs must be added before the following line
call plug#end()

" load colorscheme {{{
exec 'colorscheme '.s:settings.colorscheme

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Miscellaneous configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


" Vimpager
"let g:vimpager_passthrough = 0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sourced files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/keybindings.vim " Load custom keybindings
