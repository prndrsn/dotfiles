
" Window navigation
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Easier buffer switching
nmap <leader>1 :b1<CR>
nmap <leader>2 :b2<CR>
nmap <leader>3 :b3<CR>
nmap <leader>4 :b4<CR>
nmap <leader>5 :b5<CR>
nmap <leader>6 :b6<CR>
nmap <leader>7 :b7<CR>
nmap <leader>8 :b8<CR>
nmap <leader>9 :b9<CR>
nmap <leader>0 :b0<CR>
" Easier window switching
nmap <silent> <A-i> :wincmd k<CR>
nmap <silent> <A-k> :wincmd j<CR>
nmap <silent> <A-j> :wincmd h<CR>
nmap <silent> <A-l> :wincmd l<CR>
nmap <silent> <A-Up> <C-w><Up>
nmap <silent> <A-Down> <C-w><Down>
nmap <silent> <A-Left> <C-w><Left>
nmap <silent> <A-Right> <C-w><Right>
" Make window switching work in nvim terminal
if has("nvim")
    tnoremap <A-j> <C-\><C-n><C-w>h
    tnoremap <A-k> <C-\><C-n><C-w>j
    tnoremap <A-i> <C-\><C-n><C-w>k
    tnoremap <A-l> <C-\><C-n><C-w>l
    tnoremap <A-Left> <C-\><C-n><C-w>h
    tnoremap <A-Down> <C-\><C-n><C-w>j
    tnoremap <A-Up> <C-\><C-n><C-w>k
    tnoremap <A-Right> <C-\><C-n><C-w>l
endif

" undo
noremap <C-z> u
" Fix unwanted fucked up Insert mode switch
noremap c<Space> ""

" Bubble single lines
nmap <S-Up> ddiP
nmap <S-Down> ddp
" Bubble multiple lines
vmap <S-Up> xiP`[V`]
vmap <S-Down> xp`[V`]

" Fold/unfold
noremap <C-Left> zc
noremap <C-Right> zo
nnoremap zr zr:echo &foldlevel<cr>
nnoremap zm zm:echo &foldlevel<cr>
nnoremap zR zR:echo &foldlevel<cr>
nnoremap zM zM:echo &foldlevel<cr>

" Move forward/backward word, WORD
noremap o W
noremap O w
noremap <C-o> E
nnoremap u B
nnoremap U b
" Move up/down, insert before/after, insert newline above/below, insert
" beg/end line
noremap i gk
nnoremap I i
noremap <C-i> O
noremap k gj
noremap K a
noremap <C-k> o
noremap <C-l> A
noremap <C-j> I

" ö, h - Go to end/beginning of line with whitespace
noremap ö g$
noremap h g0
"noremap <C-h> ^

" j, l, J, L - Go left, right, beginning/end of line w/o whitespace
noremap j h
noremap J g^
" leave unchanged: l
noremap L $
" leave unchanged: :
noremap ' "


" Move paragraph end/beginning
noremap <C-Down> )
noremap <C-Up> (

" ä, Ä, å - Search forward/backward, turn off highlight
noremap ä /
noremap Ä ?
noremap å :nohlsearch<CR>

"--- GUI ---
"nnoremap <F2> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>
